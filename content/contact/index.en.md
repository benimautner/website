---
title: "Contact"
date: 2018-11-19T22:07:56+02:00
draft: false
type: "page"
---

# Contact

We have public issue trackers in the different [code repos](https://code.vikunja.io).

#### Chat

You can chat with us on [matrix](https://riot.im/app/#/room/!dCRiCiLaCCFVNlDnYs:matrix.org?via=matrix.org).

#### Forum

A community forum is available on [community.vikunja.io](https://community.vikunja.io).

#### General

[hello@vikunja.io](mailto:hello@vikunja.io)

General inqueries and requests.

#### Maintainers

[maintainers@vikunja.io](mailto:maintainers@vikunja.io)

Say hello to our maintainers.

#### Security 

When you find security-related bugs or other things you don't want to disclose publicly via our bug tracker, this is the mail to use.

[security@vikunja.io](mailto:security@vikunja.io)

**PGP-Key:** 2DD15B4BBC0FFB1AEF056662182B59A2D78D7303

You can download the public key from [here](security.pub) or [here](https://kolaente.dev/vikunja/website/raw/branch/main/content/contact/security.pub) or [here](http://keyserver.ubuntu.com/pks/lookup?search=security%40vikunja.io&fingerprint=on&op=index).
