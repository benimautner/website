---
title: "Download"
date: 2018-10-13T22:07:56+02:00
draft: false
type: "page"
menu:
  page:
    title: "Download"
    weight: 30
---

# Downloads

If you find any bugs or other issues, please [report them](https://code.vikunja.io/api/issues).

Please also check out [the docs](https://vikunja.io/docs/installing/) for information about how to install Vikunja on your own server.

You can download Vikunja in various forms:

## Backend (API-Server)

You can get all api releases from [our download server](https://dl.vikunja.io/api/).

Unstable builds contain the latest changes and features, but may be experimental or contain bugs.

You can also [get the source code](https://code.vikunja.io/api) and compile it yourself. 
Instructions on how to do this can be found in the repo.

The api is available as `.deb`, `.apk` (alpine) and `.rpm` package.
Just look for the files in the respective release folder.

## Frontend (Javascript)

You can get all frontend releases from [our download server](https://dl.vikunja.io/frontend/).

You can also [get the source code](https://code.vikunja.io/frontend) and build it yourself. Instructions on how to do this can be found in the repo.

## Desktop apps

Starting with version 0.15.0, Vikunja's frontend is available as a standalone desktop app for linux and windows devices.
This means you can run it on your desktop without the need to install the frontend.

To install it, simply go to [our download server](https://dl.vikunja.io/desktop) choose the latest version (`unstable` for nightly builds) and the right variant for your platform.

## Mobile apps

<div class="notification is-warning">
<strong>Note:</strong> The app is currently in an alpha state at best.
It has only a tiny set of Vikunja's features and bugs.
If you want to use Vikunja on mobile, you're currently better off using the PWA of the frontend.
</div>

We have [pre-built apks for Android](https://dl.vikunja.io/app/) which you can download and install on your device. 
We'll publish it to F-Droid and the Google Play Store once the app is feature complete.

The app has one [known issue](https://kolaente.dev/vikunja/app/issues/48) right now which makes it essentially unusable.
You can try [one of the recent builds from github](https://github.com/go-vikunja/app/actions/runs/1606526877), but these are untested.

Unfortunately there's not really any capacity or ressources right now for the flutter app.
If you have time and know flutter, we're happily accepting PRs on Github.

You can also [get the source code](https://github.com/go-vikunja/app) and build it yourself (for both Android and iOS).

